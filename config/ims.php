<?php

return [
    'currency' => env('IMS_CURRENCY', 'EUR'),
    'file_fsdriver' => 'file',
];
