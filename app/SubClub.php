<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubClub extends Model
{
    public function club()
    {
        return $this->belongsTo('App\Club');
    }

    public function ocupations()
    {
        return $this->hasMany('App\Ocupation');
    }

    public function price()
    {
        return $this->morphOne('App\Pricing', 'priced');
    }
}
