<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\User;
use App\Role;
use App\Participant;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => App\Policies\Dashboard\UserPolicy::class,
        Participant::class => App\Policies\Dashboard\ParticipantPolicy::class,
        Role::class => App\Policies\Dashboard\RolePolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        $gate->before(function (User $user, $ability) {
            $permissions = $user->effectivePermissions();
            if(in_array('*', $permissions)){
                return true;
            }
            foreach ($permissions as $permission) {
                if(preg_match($permission, $ability) === 1){
                    return true;
                }
            }
        });
        //
    }
}
