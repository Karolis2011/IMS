<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use App\File;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        File::creating(function($file)
        {
            $validUID = false;
            while(!$validUID) {
                $uid = substr(base64_encode(sha1(mt_rand())), 0, 16);
                $dupUID = File::where('uid', '=', $uid)->first();
                if($dupUID === null){
                    $file->uid = $uid;
                    $validUID = true;
                }
            }
        });
    }
}
