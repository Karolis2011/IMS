<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Role;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function manage()
    {
        $this->authorize('global.roles.manage');
        return view('dashboard.auth.roles.manage', [
            'roles' => Role::all(),
        ]);
    }

    public function delete(Role $role)
    {
        $this->authorize('delete', $role);
        $role->delete();
        flash_message("Success!", "Role has been deleted successfully!", "success");
        return redirect()->route('dashboard.auth.roles.index');
    }

    public function deleteConfirm(Role $role)
    {
        $this->authorize('delete', $role);
        return view('dashboard.auth.roles.delete', [
            'role' => $role,
        ]);
    }

    public function view(Role $role)
    {
        $this->authorize('edit', $role);
        return view('dashboard.auth.roles.view', [
            'role' => $role,
        ]);
    }

    public function add(Request $req)
    {
        $this->authorize('global.roles.create');
        Role::create($req->all());
        flash_message("Success!", "Role has been created successfully!", "success");
        return redirect()->route('dashboard.auth.roles.index');
    }

    public function addView()
    {
        return view('dashboard.auth.roles.add');
    }

    public function edit(Request $req, Role $role)
    {
        $this->authorize('edit', $role);
        $role->name = $req->name;
        $role->save();
        flash_message("Success!", "Role has been updated successfully!", "success");
        return redirect()->route('dashboard.auth.roles.index');
    }

    public function addPermission(Request $req, Role $role)
    {
        $this->authorize('edit', $role);
        $perms = $role->permissions;
        if(is_null($perms)){
            $perms = array();
        }
        if(!in_array($req->name, $perms))
        {
            $perms[] = $req->name;
            $role->permissions = $perms;
            $role->save();
            return 'OK';
        }
        return 'ERROR';
    }

    public function delPermission(Request $req, Role $role)
    {
        $this->authorize('edit', $role);
        $perms = $role->permissions;
        if(is_null($perms)){
            $perms = array();
        }
        if(in_array($req->name, $perms))
        {
            if(($key = array_search($req->name, $perms)) !== false) {
                unset($perms[$key]);
                if(!empty($perms)) {
                    $role->permissions = $perms;
                } else {
                    $role->permissions = null;
                }
                $role->save();
                return 'OK';
            }
        }
        return 'ERROR';
    }
}
