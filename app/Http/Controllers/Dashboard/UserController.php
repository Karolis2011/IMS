<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\User;
use App\Role;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function manage()
    {
        $this->authorize('global.users.manage');
        return view('dashboard.auth.manage', [
            'users' => User::all(),
        ]);
    }

    public function delete(User $user)
    {
        $this->authorize('delete', $user);
        $user->delete();
        flash_message("Success!", "User has been deleted successfully!", "success");
        return redirect()->route('dashboard.auth.index');
    }

    public function deleteConfirm(User $user)
    {
        $this->authorize('delete', $user);
        return view('dashboard.auth.delete', [
            'user' => $user,
        ]);
    }

    public function view(User $user)
    {
        $this->authorize('edit', $user);
        return view('dashboard.auth.view', [
            'user' => $user,
            'allRoles' => Role::all(),
        ]);
    }

    public function edit(Request $req, User $user)
    {
        $this->authorize('edit', $user);
        $user->name = $req->name;
        $user->email = $req->email;
        $user->birthday = $req->birthday;
        $user->phone = $req->phone;
        $user->save();
        flash_message("Success!", "User has been updated successfully!", "success");
        return redirect()->route('dashboard.auth.index');
    }

    public function removeRole(Request $req, User $user)
    {
        $this->authorize('edit', $user);
        $user->roles()->detach($req->id);
        return "OK";
    }

    public function addRole(Request $req, User $user)
    {
        $this->authorize('edit', $user);
        if(!$user->roles->contains(Role::find($req->id))){
            $user->roles()->attach($req->id);
            return "OK";
        }
        return 'ERROR';
    }
}
