<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Participant;
use App\User;
use App\Role;

class ParticipantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function manage()
    {
        $this->authorize('global.participants.manage');
        return view('dashboard.data.participants.manage', [
            'participants' => Participant::all(),
        ]);
    }

    public function delete(Participant $participant)
    {
        $this->authorize('delete', $participant);
        $user->delete();
        flash_message("Success!", "Participant has been deleted successfully!", "success");
        return redirect()->route('dashboard.participants.index');
    }

    public function deleteConfirm(Participant $participant)
    {
        $this->authorize('delete', $participant);
        return view('dashboard.data.participants.delete', [
            'participant' => $participant,
        ]);
    }

    public function view(Participant $participant)
    {
        $this->authorize('edit', $participant);
        return view('dashboard.data.participants.view', [
            'participant' => $participant,
            'users' => User::all()->sortBy('name'),
            'roles' => Role::all()->sortBy('name'),
        ]);
    }

    public function add(Request $req)
    {
        $this->authorize('global.participants.create');
        Participant::create($req->all());
        flash_message("Success!", "Participant has been created successfully!", "success");
        return redirect()->route('dashboard.participants.index');
    }

    public function addView(Request $req)
    {
        return view('dashboard.data.participants.add');
    }

    public function edit(Request $req, Participant $participant)
    {
        $this->authorize('edit', $participant);
        $participant->firstname = $req->firstname;
        $participant->lastname = $req->lastname;
        $participant->birthday = $req->birthday;
        $participant->gender = $req->gender;
        $participant->save();
        flash_message("Success!", "Participant has been updated successfully!", "success");
        return redirect()->route('dashboard.participants.index');
    }

    public function deleteManager(Request $req, Participant $participant)
    {
        $this->authorize('edit', $participant);
        $participant->users()->detach($req->user);
        return "OK";
    }

    public function addManager(Request $req, Participant $participant)
    {
        $this->authorize('edit', $participant);
        $participant->users()->attach($req->user, ['role_id' => $req->role]);
        flash_message("Success!", "Manager for participant has been added successfully!", "success");
        return redirect()->route('dashboard.participants.view', $participant);
    }
}
