<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Cache;

use App\File;

class FileController extends Controller
{
    public function getImage(Request $req, $imageUID)
    {

    }

    public function uploadFile(Request $req)
    {
        if ($req->hasFile('file')) {
            if ($req->file('file')->isValid()) {
                $file_contents = file_get_contents($req->file('file')->getRealPath());
                $file = new File();
                if($req->file('file')->getClientOriginalName() === null){
                    $file->filename = "file" . $req->file('file')->getExtension();
                } else {
                    $file->filename = preg_replace('/[^A-Za-z0-9\-\.]/', '', $req->file('file')->getClientOriginalName());
                }
                $file->save();
                Storage::disk(config('ims.file_fsdriver'))->put($file->uid . '.data', $file_contents);

            }
        }
    }

    public function generateThumbnail(Request $req, $dimensions)
    {
        if(isset($req->url)){
            $cacheHash = 'thumbnail:' . hash('md5', $dimensions . $req->url);
            if (Cache::has($cacheHash)) {
                return response(Cache::get($cacheHash), 200)
                    ->header('Content-Type', 'image/png');
            } else {
                $imageData = file_get_contents($req->url);
                $myImage = imagecreatefromstring($imageData);
                if (is_resource($myImage) === true){
                    $size = explode("x", $dimensions);
                    $thumbnail_width = $size[0];
                    $thumbnail_height = $size[1];
                    list($width_orig, $height_orig) = getimagesizefromstring($imageData);
                    //$new_image = imagecreatetruecolor($new_width, $new_height);

                    $ratio_orig = $width_orig/$height_orig;

                    if ($thumbnail_width/$thumbnail_height > $ratio_orig) {
                        $new_height = $thumbnail_width/$ratio_orig;
                        $new_width = $thumbnail_width;
                    } else {
                        $new_width = $thumbnail_height*$ratio_orig;
                        $new_height = $thumbnail_height;
                    }

                    $x_mid = $new_width/2;  //horizontal middle
                    $y_mid = $new_height/2; //vertical middle

                    $process = imagecreatetruecolor(round($new_width), round($new_height));

                    imagecopyresampled($process, $myImage, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
                    $thumb = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
                    imagecopyresampled($thumb, $process, 0, 0, ($x_mid-($thumbnail_width/2)), ($y_mid-($thumbnail_height/2)), $thumbnail_width, $thumbnail_height, $thumbnail_width, $thumbnail_height);


                    //imagecopyresampled($new_image, $image, 0, 0, $src_x, $src_y, $crop_w, $crop_h, $old_width, $old_height);
                    ob_start();
                    imagepng($thumb);
                    $imagerawdata = ob_get_clean();
                    imagedestroy($myImage);
                    imagedestroy($process);
                    imagedestroy($thumb);
                    Cache::put($cacheHash, $imagerawdata, 10);
                    return response($imagerawdata, 200)
                        ->header('Content-Type', 'image/png');
                } else {
                    abort(404);
                }
            }
        } else {
            abort(404);
        }
    }
}
