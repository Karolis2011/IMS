<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Club;
use App\SubClub;

class ClubController extends Controller
{
    public function viewList()
    {
        //return Club::where('public', true)->get();
        return view('data.club.list', [
            'clubs' => Club::where('public', true)->get(),
        ]);
    }

    public function viewClub(Club $club)
    {
        return view('data.club.view', [
            'club' => $club,
        ]);
    }

    public function viewSubClub(SubClub $subclub)
    {
        return view('data.subclub.view', [
            'subclub' => $subclub,
        ]);
    }
}
