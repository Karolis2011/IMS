<?php

namespace App\Http\Controllers\API\AJAX;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;

use Auth;

class SearchController extends Controller
{
    public function usersByName(Request $req)
    {
        if (!Auth::check()) {
            abort(401);
        }
        $users = User::where('name', 'LIKE', '%' . $req->name . '%')->select(array('id', 'name', 'email'))->get();
        return $users;
    }
}
