<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/lang/{lang}', function ($lang)
{
    session(['lang' => $lang]);
    return redirect()->route('auth.login');
});

Route::group(['prefix' => 'file', 'as' => 'file.'], function () {
    Route::get('/thumb/{dimensions}', ['as' => 'thumbnail', 'uses' => 'FileController@generateThumbnail']);
});

Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.'], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'DashboardController@index']);
    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function ()
    {
        Route::group(['prefix' => 'role', 'as' => 'roles.'], function ()
        {
            Route::get('/', ['as' => 'index', 'uses' => 'Dashboard\RoleController@manage']);
            Route::post('/add', ['as' => 'add', 'uses' => 'Dashboard\RoleController@add']);
            Route::get('/add', ['as' => 'addform', 'uses' => 'Dashboard\RoleController@addView']);
            Route::delete('/{role}', ['as' => 'delete', 'uses' => 'Dashboard\RoleController@delete']);
            Route::get('/{role}/delete', ['as' => 'deleteConfirm', 'uses' => 'Dashboard\RoleController@deleteConfirm']);
            Route::get('/{role}', ['as' => 'view', 'uses' => 'Dashboard\RoleController@view']);
            Route::post('/{role}', ['as' => 'edit', 'uses' => 'Dashboard\RoleController@edit']);
            Route::delete('/{role}/perm', ['as' => 'delperm', 'uses' => 'Dashboard\RoleController@delPermission']);
            Route::post('/{role}/perm', ['as' => 'addperm', 'uses' => 'Dashboard\RoleController@addPermission']);
        });
        Route::get('/', ['as' => 'index', 'uses' => 'Dashboard\UserController@manage']);
        Route::delete('/{user}', ['as' => 'delete', 'uses' => 'Dashboard\UserController@delete']);
        Route::get('/{user}/delete', ['as' => 'deleteConfirm', 'uses' => 'Dashboard\UserController@deleteConfirm']);
        Route::get('/{user}', ['as' => 'view', 'uses' => 'Dashboard\UserController@view']);
        Route::post('/{user}', ['as' => 'edit', 'uses' => 'Dashboard\UserController@edit']);
        Route::delete('/{user}/role', ['as' => 'delrole', 'uses' => 'Dashboard\UserController@removeRole']);
        Route::post('/{user}/role', ['as' => 'addrole', 'uses' => 'Dashboard\UserController@addRole']);
    });
    Route::group(['prefix' => 'participant', 'as' => 'participants.'], function() {
        Route::get('/', ['as' => 'index', 'uses' => 'Dashboard\ParticipantController@manage']);
        Route::post('/add', ['as' => 'add', 'uses' => 'Dashboard\ParticipantController@add']);
        Route::get('/add', ['as' => 'addform', 'uses' => 'Dashboard\ParticipantController@addView']);
        Route::delete('/{participant}', ['as' => 'delete', 'uses' => 'Dashboard\ParticipantController@delete']);
        Route::get('/{participant}/delete', ['as' => 'deleteConfirm', 'uses' => 'Dashboard\ParticipantController@deleteConfirm']);
        Route::get('/{participant}', ['as' => 'view', 'uses' => 'Dashboard\ParticipantController@view']);
        Route::post('/{participant}', ['as' => 'edit', 'uses' => 'Dashboard\ParticipantController@edit']);
        Route::post('/{participant}/manager', ['as' => 'addmanager', 'uses' => 'Dashboard\ParticipantController@addManager']);
        Route::delete('/{participant}/manager', ['as' => 'delmanager', 'uses' => 'Dashboard\ParticipantController@deleteManager']);
    });
});

Route::group(['prefix' => 'account', 'as' => 'auth.'], function () {
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@showLoginForm']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@login']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);

    // Registration Routes...
    Route::get('register', ['as' => 'register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
    Route::post('register', ['as' => 'register', 'uses' => 'Auth\AuthController@register']);

    // Password Reset Routes...
    Route::get('password/reset/{token?}', ['as' => 'password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
    Route::post('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\PasswordController@reset']);
});

/*Route::group(['prefix' => 'api', 'as' => 'api.'], function () {
    Route::group(['prefix' => 'ajax', 'as' => 'ajax'], function() {
        Route::group(['prefix' => 'search', 'as' => 'search.'], function () {
            Route::get('user', ['as' => 'user', 'uses' => 'API\AJAX\SearchController@usersByName']);
        });
    });
});*/

Route::get('/', ['uses' => 'ClubController@viewList']);
Route::get('/subclub/{subclub}', ['as' => 'viewSubClub', 'uses' => 'ClubController@viewSubClub']);
Route::get('/{club}', ['as' => 'view', 'uses' => 'ClubController@viewClub']);
