<?php
function flash_message($title, $message, $type)
{
    Request::session()->flash('message_title', $title);
    Request::session()->flash('message_message', $message);
    Request::session()->flash('message_type', $type);
}
