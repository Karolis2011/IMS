<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    public function subClubs()
    {
        return $this->hasMany('App\SubClub');
    }

    public function departiment()
    {
        return $this->belongsTo('App\Departiment');
    }
}
