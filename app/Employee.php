<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function ocupations()
    {
        return $this->hasMany('App\Ocupation');
    }
}
