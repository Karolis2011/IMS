<?php

namespace App\Policies\Dashboard;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\Role;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit(User $editor, Role $role)
    {
        $permissions = $editor->effectivePermissions();
        foreach ($permissions as $permission) {
            if(preg_match($permission, 'global.roles.edit') === 1){
                return true;
            }
        }
    }

    public function delete(User $editor, Role $role)
    {
        if($editor->id == $user->id){
            return true;
        }
        $permissions = $editor->effectivePermissions();
        foreach ($permissions as $permission) {
            if(preg_match($permission, 'global.roles.delete') === 1){
                return true;
            }
        }
    }
}
