<?php

namespace App\Policies\Dashboard;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;
use App\Participant;

class ParticipantPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit(User $editor, Participant $participant)
    {
        $permissions = $editor->effectivePermissions();
        foreach ($permissions as $permission) {
            if(preg_match($permission, 'global.participants.edit') === 1){
                return true;
            }
        }
    }

    public function delete(User $editor, Participant $participant)
    {
        foreach ($permissions as $permission) {
            if(preg_match($permission, 'global.participants.delete') === 1){
                return true;
            }
        }
    }
}
