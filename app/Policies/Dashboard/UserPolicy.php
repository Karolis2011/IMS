<?php

namespace App\Policies\Dashboard;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\User;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit(User $editor, User $user)
    {
        if($editor->id == $user->id){
            return true;
        }
        $permissions = $editor->effectivePermissions();
        foreach ($permissions as $permission) {
            if(preg_match($permission, 'global.users.edit') === 1){
                return true;
            }
        }
    }

    public function delete(User $editor, User $user)
    {
        if($editor->id == $user->id){
            return true;
        }
        $permissions = $editor->effectivePermissions();
        foreach ($permissions as $permission) {
            if(preg_match($permission, 'global.users.delete') === 1){
                return true;
            }
        }
    }


}
