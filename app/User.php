<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'birthday', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'permissions' => 'array'
    ];

    public function roles() {
        return $this->belongsToMany('App\Role');
    }

    public function participants()
    {
        return $this->belongsToMany('App\Participant')->withPivot('role_id');
    }

    public function effectivePermissions($force = false)
    {
        $permList = $this->permissions;
        if(is_null($permList)){
            $permList = array();
        }
        foreach ($this->roles as $role) {
            $rolePermissions = $role->permissions;
            if(is_array($rolePermissions)){
                $permList = array_merge($permList, $rolePermissions);
            }
        }
        return array_unique($permList);
    }
}
