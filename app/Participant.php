<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Participant extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'firstname', 'lastname', 'birthday', 'gender',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('role_id');
    }

    public function name()
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
