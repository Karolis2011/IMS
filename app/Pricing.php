<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    protected $casts = [
        'price' => 'array'
    ];

    public function priced()
    {
        return $this->morphTo();
    }

    public function getPrice($ammount)
    {
        $avavible = array_filter(array_keys($this->price), function($val) use (&$ammount) {
            return $val <= $ammount;
        });
        $priceIndex = max($avavible);
        return $this->price[$priceIndex] * $ammount;
    }
}
