<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ocupation extends Model
{
    use SoftDeletes;

    public function subClub()
    {
        return $this->belongsTo('App\SubClub');
    }

    public function host()
    {
        return $this->belongsTo('App\Employee');
    }

    public function participations()
    {
        return $this->hasMany('App\Participation');
    }
}
