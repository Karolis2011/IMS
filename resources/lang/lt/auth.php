<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Šitie kredencialai nesutampa mūsų įrašais.',
    'throttle' => 'Perdaug prisijungimo bandymų. Prašome pabandyti po :seconds sekundžių.',
    'email_field' => 'El. Pašto addressas',
    'password_field' => 'Slaptažodis',
    'confpassword_field' => 'Patvirtinkite Slaptažodį',
    'rememeber' => 'Atsiminti mane',
    'forgot' => 'Pamiršote slaptažodį?',
    'login' => 'Prisijungti',
    'login_action' => 'Prisijungti',

    'register' => 'Registruotis',
    'register_action' => 'Registruotis',
    'name_field' => 'Vardas, Pavardė',
    'birthday_field' => 'Gimimo data',
    'phone_field' => 'Telefonas'
];
