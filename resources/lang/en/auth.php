<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email_field' => 'E-Mail Address',
    'password_field' => 'Password',
    'confpassword_field' => 'Confirm Password',
    'rememeber' => 'Remember Me',
    'forgot' => 'Forgot Your Password?',
    'login' => 'Login',
    'login_action' => 'Login',

    'register' => 'Register',
    'register_action' => 'Register',
    'name_field' => 'Name',
    'birthday_field' => 'Birthday',
    'phone_field' => 'Phone'
];
