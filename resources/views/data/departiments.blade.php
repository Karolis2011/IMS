@extends('layouts.app')

@section('title', "Departiments")

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @forelse($clubs as $club)
                <div class="thumbnail col-md-4 col-sm-6 col-xs-12">
                    <img src="http://placehold.it/350x250" alt="Placeholder">
                    <div class="caption">
                        <h3>{{ $club->name }}</h3>
                        <p>{{ $club->shortDescription }}</p>
                        <p><a href="{{ route('view', $club->id) }}" class="btn btn-primary" role="button">Details</a>
                    </div>
                </div>
            @empty
                <p>Sorry, there are no active clubs.</p>
            @endforelse
        </div>
    </div>
</div>
@endsection
