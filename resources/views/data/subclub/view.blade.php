@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 panel panel-default">
            <div class="panel-body">
                <?php $hours = intval($subclub->duration/60); $minutes = $subclub->duration - ($hours * 60); ?>
                <h2>{{ $subclub->name }}</h2>
                <p>{!! $subclub->description !!}</p>
                <dl class="dl-horizontal">
                    <dt>Weekday</dt>
                    <dd>{{ $subclub->weekday }}</dd>
                    <dt>Start Time</dt>
                    <dd>{{ $subclub->starttime }}</dd>
                    <dt>Duration</dt>
                    @if($minutes == 0)
                        <dd>{{ $hours }} hours</dd>
                    @else
                        <dd>{{ $hours }} hours {{ $minutes }} minutes</dd>
                    @endif
                    <dt>Price</dt>
                    <dd>{{ $subclub->price->getPrice(1) }} {{ config('ims.currency') }}</dd>
                </dl>
            </div>
        </div>
    </div>
</div>
@endsection
