@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 panel panel-default">
            <div class="panel-body club-info">
                <div class="club-image" style="background-image: url('{{ $club->imageUrl }}');">
                    <div class="club-title">{{ $club->name }}</div>
                </div>
                <p/>
                <p>{!! $club->description !!}</p>
                @forelse($club->subClubs as $subclub)
                    {{-- $subclub --}}
                    <div class="panel panel-default col-md-4 col-sm-6 col-xs-12">
                        <div class="panel-body">
                            <h3>{{ $subclub->name }}</h3>
                            <p>{!! $subclub->shortDescription !!}</p>
                            <?php $hours = intval($subclub->duration/60); $minutes = $subclub->duration - ($hours * 60); ?>
                            <p>
                                @if($minutes === 0)
                                    Club starts at {{ $subclub->starttime }} ({{ $subclub->weekday }}) runs for {{ $hours }} hours.
                                @else
                                    Club starts at {{ $subclub->starttime }} ({{ $subclub->weekday }}) runs for {{ $hours }} hours and {{ $minutes }} minutes.
                                @endif
                            </p>
                            <p><a href="{{ route('viewSubClub', $subclub->id) }}" class="btn btn-primary" role="button">Details</a>
                        </div>
                    </div>
                @empty
                    <p>Sorry, there are no active sub clubs in this club.</p>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection
