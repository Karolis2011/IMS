@extends('layouts.app')

@section('beforeContent')
    <div class="content">
        {{--<div class="row">--}}
            <nav class="col-md-2 col-md-offset-1" style="max-width: 100%;">
                <ul class="nav nav-pills nav-stacked">
                    <li {{ Request::route()->getName() == 'dashboard.index' ? 'class=active' : ''}}><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
                    <hr>
                    Admin things:
                    <li {{ (0 ===  strpos(Request::route()->getName(), 'dashboard.auth.') && !(0 ===  strpos(Request::route()->getName(), 'dashboard.auth.roles.'))) ? 'class=active' : ''}}><a href="{{ route('dashboard.auth.index') }}">User Management</a></li>
                    <li {{ 0 ===  strpos(Request::route()->getName(), 'dashboard.auth.roles.') ? 'class=active' : '' }}><a href="{{ route('dashboard.auth.roles.index') }}">Role Management</a></li>
                    <li {{ 0 ===  strpos(Request::route()->getName(), 'dashboard.participants.') ? 'class=active' : '' }}><a href="{{ route('dashboard.participants.index') }}">Participant Management</a></li>
                </ul>
                </ul>
            </nav>
            @yield('content2')
        {{--</div>--}}
    </div>
@endsection

@section('title')
    @hasSection('title2')
        @yield('title2') - Dashboard
    @else
        Dashboard
    @endif
@endsection
