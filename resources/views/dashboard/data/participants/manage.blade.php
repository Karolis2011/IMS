@extends('layouts.dashboard')

@section('content2')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Participant Management</div>

                <div class="panel-body">
                    <div class="btn-group" style="margin-bottom:10px;">
                        <a href="{{ route('dashboard.participants.add')}}" class="btn btn-default">Add participant</a>
                    </div>
                    <table class="table">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Birthday</th>
                            <th></th>
                         </tr>
                        @foreach ($participants as $participant)
                            <tr>
                                <td>{{ $participant->id }}</td>
                                <td>{{ $participant->firstname . ' ' . $participant->lastname }}</td>
                                <td>{{ $participant->gender }}</td>
                                <td>{{ $participant->birthday }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                            <span class="glyphicon glyphicon-cog"></span><span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="{{ route('dashboard.participants.view', $participant->id) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>Edit</a></li>
                                            <li role="presentation"><a role="menuitem" href="{{ route('dashboard.participants.deleteConfirm', $participant->id) }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
