@extends('layouts.dashboard')

@section('content2')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Delete role '{{ $role->name }}'</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('dashboard.auth.roles.delete', $role->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <div class="col-md-6 col-md-offset-2">
                            Are you sure you want to delete role '{{ $role->name }}'?
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-btn fa-user"></i> Yes
                                </button>
                                <a href="{{ route('dashboard.auth.roles.index') }}" class="btn btn-default">
                                    <i class="fa fa-btn fa-user"></i> No
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
