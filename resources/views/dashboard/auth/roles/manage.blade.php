@extends('layouts.dashboard')

@section('content2')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Role Management</div>

                <div class="panel-body">
                    <div class="btn-group" style="margin-bottom:10px;">
                        <a href="{{ route('dashboard.auth.roles.add')}}" class="btn btn-default">Add role</a>
                    </div>
                    <table class="table" style="table-layout: fixed;">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Permissions</th>
                            <th></th>
                         </tr>
                        @foreach ($roles as $role)
                            <tr>
                                <td>{{ $role->id }}</td>
                                <td>{{ $role->name }}</td>
                                <td style="word-wrap: break-word;">{{ json_encode($role->permissions) }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                            <span class="glyphicon glyphicon-cog"></span><span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="{{ route('dashboard.auth.roles.view', $role->id) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>Edit</a></li>
                                            <li role="presentation"><a role="menuitem" href="{{ route('dashboard.auth.roles.deleteConfirm', $role->id) }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
