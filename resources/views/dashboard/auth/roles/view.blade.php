@extends('layouts.dashboard')

@section('content2')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Edit role {{ $role->name }}</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('dashboard.auth.roles.edit', $role->id) }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $role->name }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Permissions</label>

                            <div class="col-md-6">
                                @if(isset($role->permissions))
                                    @foreach($role->permissions as $perm)
                                        <li>{{ $perm }} <a href="#" class="btn btn-danger btn-xs" onclick="$.ajax({
        url: '{{ route('dashboard.auth.roles.delperm', $role->id) }}' + '?name={{ $perm }}',
        type: 'DELETE',
        success: function(result) {
            location.reload();
        }
    }); return true;"><span class="glyphicon glyphicon-trash"></span></a></li>
                                    @endforeach
                                @else
                                    No permissions are set.
                                @endif
                                <br>
                                <div class="input-group">
                                    <input id="permission" type="text" class="form-control" name="permission">
                                    <span class="input-group-btn">
                                        <a id="permissionadd" class="btn btn-primary" onclick="$.ajax({
        url: '{{ route('dashboard.auth.roles.addperm', $role->id) }}',
        type: 'POST',
        data: {name: $('#permission').val()},
        success: function(result) {
            location.reload();
        }
    }); return true;" href="#">Add Permission</a>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
