@extends('layouts.dashboard')

@section('content2')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">User Management</div>

                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Birthday</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th></th>
                         </tr>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->birthday }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                            <span class="glyphicon glyphicon-cog"></span><span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                            <li role="presentation"><a role="menuitem" href="{{ route('dashboard.auth.view', $user->id) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>Edit</a></li>
                                            <li role="presentation"><a role="menuitem" href="{{ route('dashboard.auth.deleteConfirm', $user->id) }}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
