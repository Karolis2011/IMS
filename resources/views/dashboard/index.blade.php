@extends('layouts.dashboard')

@section('content2')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget iaculis orci. Vestibulum sit amet lorem eget tortor pretium pretium a vitae arcu. Donec et nunc nunc. Nulla eu ultricies augue, in volutpat ipsum. Aliquam congue elit libero, sit amet commodo enim laoreet sit amet. Vivamus ut arcu non diam varius pretium. Cras id nibh erat. Nam eget quam ante. Nunc dui turpis, tincidunt at ullamcorper ut, consequat et turpis. Maecenas dignissim eu ipsum id efficitur. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent sit amet nunc libero. Aenean varius tellus justo, sed mollis massa ullamcorper id. Aenean tellus quam, ultrices ac egestas ac, imperdiet in lacus. Suspendisse sed faucibus lacus, in maximus felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.

Vivamus eu pretium magna. In hac habitasse platea dictumst. Nunc id eleifend urna. Fusce et ultrices ipsum. Suspendisse pharetra risus quis maximus accumsan. Integer et malesuada elit, vel dictum erat. Quisque lectus ipsum, lacinia in turpis a, rutrum tincidunt eros. In ornare nulla sit amet turpis vehicula vulputate. Praesent id urna efficitur, elementum nibh sed, laoreet risus.

Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla lacus ipsum, dictum ac imperdiet sit amet, eleifend vitae felis. Nam lobortis, ex at placerat mollis, ligula est lacinia nulla, at ornare neque justo sit amet lorem. Morbi at pharetra nunc. Duis iaculis justo sed nulla facilisis lobortis. Curabitur vel ligula lectus. Pellentesque non sollicitudin quam, a ultrices dolor. Mauris vestibulum hendrerit mauris, vel cursus risus viverra vitae. Suspendisse efficitur tincidunt nibh, et consectetur arcu interdum sit amet. In interdum venenatis tempus. Pellentesque sit amet commodo erat. Aenean consectetur enim bibendum, rutrum ex eget, finibus neque.

Aliquam erat volutpat. Duis quis tincidunt metus. Aenean nibh ligula, interdum a quam ut, consectetur finibus lacus. Curabitur dictum gravida eros, eget accumsan ipsum placerat et. Phasellus euismod placerat elit eu consequat. Cras eget mauris malesuada, sollicitudin nulla at, aliquam quam. Cras congue lectus dolor, at efficitur enim fermentum et. Morbi condimentum dictum neque eu aliquet. Cras dapibus, tellus sed condimentum rhoncus, orci lectus sollicitudin lectus, sed finibus elit ex ut sapien. Pellentesque venenatis nisi at massa volutpat, sed pellentesque eros aliquet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum id erat neque. Nam fringilla placerat lectus, ac vehicula ante vulputate eu. Mauris urna urna, pharetra et efficitur vitae, ornare sit amet justo. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.

Proin euismod euismod nibh sit amet auctor. Ut dictum pharetra eros, aliquet consectetur felis laoreet at. Etiam diam metus, egestas et dapibus a, suscipit vitae lorem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent ac accumsan odio. Curabitur vulputate ipsum turpis. Cras in pharetra nunc. Suspendisse sit amet libero cursus, mattis eros quis, mattis ligula. Sed ornare ipsum nec nunc ultrices mattis.

Duis lectus odio, ullamcorper a fringilla a, condimentum a mauris. Nulla placerat velit ut ultricies tincidunt. Nullam felis turpis, dapibus nec massa ut, laoreet ultrices arcu. Ut ut mauris vitae arcu condimentum interdum. Nulla facilisi. Sed fermentum, velit ac molestie pulvinar, sem libero tempor neque, tempor blandit justo metus non felis. Pellentesque non lacus malesuada erat tempus consequat pharetra ut nibh. Nunc laoreet eget urna ut vestibulum. Maecenas dapibus id dui in porta. Donec dictum tempus eros, ut semper nunc. Nunc volutpat quis lectus a sodales. Aliquam auctor risus vitae hendrerit molestie. Fusce in augue at turpis euismod lacinia. Sed elementum, felis ut tristique lobortis, elit ante elementum magna, quis fermentum lectus diam id tellus. Curabitur fermentum, lorem id accumsan dictum, nunc lectus mollis risus, a lacinia orci turpis sit amet metus.

Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed id massa purus. Sed nec lorem a nisi malesuada posuere. Cras facilisis euismod dolor. In et dictum sapien. Vestibulum a lectus maximus, semper est nec, varius erat. Nam in vestibulum magna, non efficitur quam. Quisque mollis pulvinar magna vel semper.

Pellentesque turpis ex, gravida gravida vehicula at, scelerisque nec ligula. Curabitur nec velit vitae nibh gravida convallis sed nec odio. Nulla scelerisque tellus ligula, ac tempor ligula venenatis id. Mauris velit arcu, euismod ut varius eget, consequat ut quam. Etiam scelerisque quam felis. In et dolor sit amet sapien vestibulum viverra sit amet finibus diam. Donec maximus aliquam nibh sit amet tempus. Vestibulum purus libero, ornare sed ornare et, viverra eu libero. Nulla posuere erat et magna sodales tincidunt. Quisque porta nulla elit, ac ultrices dui consectetur nec. Maecenas tincidunt elementum nisl, eu porta sapien tempus ut. Nunc pretium blandit nisi ac posuere. Praesent pretium sodales odio, non vehicula urna malesuada vitae. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
