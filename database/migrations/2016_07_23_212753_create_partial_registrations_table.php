<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartialRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partial_registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('gender');
            $table->date('birthday');
            $table->string('school')->nullable();
            $table->integer('sub_club_id');
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });

        Schema::table('participants', function (Blueprint $table) {
            $table->string('school')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partial_registrations');
        Schema::table('participants', function (Blueprint $table) {
            $table->dropColumn('school');
        });
    }
}
