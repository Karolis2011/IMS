<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOcupationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocupations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('shortDescription');
            $table->dateTime('startdate');
            $table->integer('duration');
            $table->string('location')->nullable();
            $table->integer('sub_club_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->enum('status', ['planned', 'cancelled', 'executed'])->default('planned');
            $table->date('uppaydate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ocupations');
    }
}
