<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('gender');
            $table->date('birthday');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('participant_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('participant_id');
            $table->integer('role_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('participants');
        Schema::drop('participant_user');
    }
}
