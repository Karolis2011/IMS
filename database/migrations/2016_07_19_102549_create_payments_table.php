<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->float('ammount');
            $table->string('currency');
            $table->string('description');
            $table->enum('status', ['pending', 'paid', 'cancelled'])->default('pending');
            $table->integer('user_id')->nullable();
            $table->integer('participant_id');
            $table->integer('sub_club_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
